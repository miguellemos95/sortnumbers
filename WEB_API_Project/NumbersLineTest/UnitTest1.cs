using Moq;  
using NumbersOrdering.Data;  
using NumbersOrdering.Models; 
using NumbersOrdering.Dtos;  
using NumbersOrdering.Controllers; 
using Xunit;  
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

namespace NumbersLineTest
{
    public class UnitTest1
    {
        public Mock<INumbersLineRepository> mock = new Mock<INumbersLineRepository>();  

        [Fact]
        public void getNumbers()
        {
            NumbersLine numbersLine = new NumbersLine();
            NumbersLineReadDto numbersLineReadDto = new NumbersLineReadDto();

            int[] myNum = {10, 20, 30, 40};
            numbersLine.numbers = myNum;

            mock.Setup(p => p.GetNumbersLine()).Returns(numbersLine);

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<NumbersLine, NumbersLineReadDto>();
                cfg.CreateMap<NumbersLineReadDto, NumbersLine>();
            });

            IMapper _mapper = config.CreateMapper(); 
            NumbersLineController NumbersLineController = new NumbersLineController(mock.Object, _mapper); 
           
            var actionResult = NumbersLineController.getNumbers();  
            var result = (OkObjectResult) actionResult.Result;
            var numbers = (NumbersLineReadDto) result.Value;
            Assert.Equal(numbersLine.numbers, numbers.numbers); 
        }

        [Fact]
        public void postNumbers()
        {
            NumbersLinePostDto numbersLinePostDto = new NumbersLinePostDto();
            NumbersLine expectNumbersLine = new NumbersLine();
            int[] array = {20, 10, 30, 40};

            int[] sortArray = {10, 20, 30, 40};

            numbersLinePostDto.numbers = array;
            expectNumbersLine.numbers = sortArray;

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<NumbersLine, NumbersLinePostDto>();
                cfg.CreateMap<NumbersLinePostDto, NumbersLine>();
            });

            IMapper _mapper = config.CreateMapper(); 
 
            NumbersLineController NumbersLineController = new NumbersLineController(mock.Object, _mapper); 
           
            var actionResult = NumbersLineController.postNumbers(numbersLinePostDto);  
            var result = actionResult.Result as OkObjectResult;
            NumbersLine resultNumbersLine = (NumbersLine) result.Value;
            Assert.Equal(expectNumbersLine.numbers, resultNumbersLine.numbers); 
        }

    }
}
