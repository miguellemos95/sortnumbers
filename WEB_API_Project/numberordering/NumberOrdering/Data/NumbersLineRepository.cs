using System;
using System.IO;
using System.Text;
using NumbersOrdering.Models;

namespace NumbersOrdering.Data
{
    public class NumbersLineRepository : INumbersLineRepository
    {
        string storage_path = "";

        public NumbersLineRepository(){
        
            storage_path = Path.Join(Directory.GetCurrentDirectory(), "\\files\\");
            
            if (!Directory.Exists(storage_path))
            {
                Directory.CreateDirectory(storage_path);
            }
            
            
        }

        private string GetFileName(){
            return storage_path+"FileNumbers.txt";
        }

        public NumbersLine PostNumbers(NumbersLine numbersLine)
        {
            if (numbersLine == null)
            {
                return null;
            }

            string file_name = GetFileName();
            string strNumbers = String.Join(",", numbersLine.numbers);
            
            using (FileStream fs = File.Create(file_name))
            {
                byte[] info = new UTF8Encoding(true).GetBytes(strNumbers);
                
                fs.Write(info, 0, info.Length);
                fs.Close();
            }
            
            return numbersLine;
        }

        public NumbersLine GetNumbersLine()
        {   
            string file_name = GetFileName();
            NumbersLine numbersLine = new NumbersLine();

            try
            {
                numbersLine.numbers = Array.ConvertAll(File.ReadAllText(file_name).Split(","), Int32.Parse);
            }
            catch(Exception)
            {
                return new NumbersLine {numbers = null}; 
            }

            return numbersLine; 
        }
    }
}