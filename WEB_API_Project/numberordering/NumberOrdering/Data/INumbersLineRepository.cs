using System.Collections.Generic;
using NumbersOrdering.Models;


namespace NumbersOrdering.Data
{
    public interface INumbersLineRepository
    {
        NumbersLine GetNumbersLine();

        NumbersLine PostNumbers(NumbersLine numbersLine);
    }
}