using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using NumbersOrdering.Models;
using NumbersOrdering.Data;
using NumbersOrdering.Dtos;
using AutoMapper;

namespace NumbersOrdering.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NumbersLineController : ControllerBase
    {
        private readonly INumbersLineRepository _repository;
        private readonly IMapper _mapper;

        public NumbersLineController(INumbersLineRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult <NumbersLineReadDto> getNumbers()
        {
            NumbersLine numbersLine = _repository.GetNumbersLine();
            NumbersLineReadDto numbersLineReadDto = _mapper.Map<NumbersLineReadDto>(numbersLine);
            return Ok(numbersLineReadDto);
        }

        [HttpPost]
        public ActionResult <NumbersLinePostDto> postNumbers(NumbersLinePostDto numbersLinePostDto)
        {   
            NumbersLine numbersLine = _mapper.Map<NumbersLine>(numbersLinePostDto);
            NumbersLine bubbleArray = numbersLine;
            NumbersLine quickArray = numbersLine;
            NumbersLine mergeArray = numbersLine;

            bubbleArray.bubbleSort();
            quickArray.quickSort();
            mergeArray.mergeSort();

            NumbersLine response = null;
            if(bubbleArray.numbers.Equals(quickArray.numbers))
            {
                response = bubbleArray;
                response.quickSortTime = quickArray.quickSortTime;
                response.mergeSortTime = mergeArray.mergeSortTime;
                _repository.PostNumbers(response);
            }

            return Ok(response);
        }
    }
}