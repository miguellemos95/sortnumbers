using System.Collections.Generic;
using System;
using System.IO;
using System.Text;

namespace NumbersOrdering.Models
{
    public class NumbersLine
    {
        public int[] numbers{get; set;}
        public long bubbleSortTime{get;set;}
        public long quickSortTime{get;set;}
        public long mergeSortTime{get;set;}
        
        public void bubbleSort(){
            var watch = new System.Diagnostics.Stopwatch();
            int n;
            
            watch.Start();
            for (n = numbers.Length; n >= 2; n--)
            {
                for (int j = 0; j <= n - 2; j++)
                {
                    if (numbers[j] > numbers[j + 1])
                    {
                        int tmp = numbers[j + 1];
                        numbers[j + 1] = numbers[j];
                        numbers[j] = tmp;
                    }
                } 
            }
            watch.Stop();

            bubbleSortTime = watch.ElapsedMilliseconds;
        }

        public void quickSort(){
            var watch = System.Diagnostics.Stopwatch.StartNew();

            Quick_Sort(numbers, 0, numbers.Length - 1);
            watch.Stop();

            quickSortTime = watch.ElapsedMilliseconds;
        }

        private static void Quick_Sort(int[] arr, int left, int right) 
        {
            if (left < right)
            {
                int pivot = Partition(arr, left, right);

                if (pivot > 1) 
                {
                    Quick_Sort(arr, left, pivot - 1);
                }
                if (pivot + 1 < right)
                {
                    Quick_Sort(arr, pivot + 1, right);
                }
            }
        
        }

        private static int Partition(int[] arr, int left, int right)
        {
            int pivot = arr[left];
            while (true) 
            {

                while (arr[left] < pivot) 
                {
                    left++;
                }

                while (arr[right] > pivot)
                {
                    right--;
                }

                if (left < right)
                {
                    if (arr[left] == arr[right]) return right;

                    int temp = arr[left];
                    arr[left] = arr[right];
                    arr[right] = temp;
                }
                else 
                {
                    return right;
                }
            }
        }

        
        public void mergeSort(){
            var watch = new System.Diagnostics.Stopwatch();

            watch.Start();
            numbers = mergeSort(numbers);
            watch.Stop();

            mergeSortTime = watch.ElapsedMilliseconds;
        }

        private static int[] mergeSort(int[] array)
        {
            int[] left, right, result;  
            
            if (array.Length <= 1)
            {
                return array;              
            }

            int midPoint = array.Length / 2;  
            
            left = new int[midPoint];
  
            if (array.Length % 2 == 0)
            {
                right = new int[midPoint];  
            }
            else
            {
                right = new int[midPoint + 1]; 
            } 
            
            for (int i = 0; i < midPoint; i++)
            {
                left[i] = array[i];  
            }
            
            int x = 0;
            
            for (int i = midPoint; i < array.Length; i++)
            {
                right[x] = array[i];
                x++;
            }  
            
            left = mergeSort(left);
            right = mergeSort(right);     
            result = merge(left, right);  
            
            return result;
        }
  
        private static int[] merge(int[] left, int[] right)
        {
            int resultLength = right.Length + left.Length;
            int[] result = new int[resultLength];
            
            int indexLeft = 0, indexRight = 0, indexResult = 0;  
            
            while (indexLeft < left.Length || indexRight < right.Length)
            {
                if (indexLeft < left.Length && indexRight < right.Length)  
                {  
                    if (left[indexLeft] <= right[indexRight])
                    {
                        result[indexResult] = left[indexLeft];
                        indexLeft++;
                        indexResult++;
                    }
                    
                    else
                    {
                        result[indexResult] = right[indexRight];
                        indexRight++;
                        indexResult++;
                    }
                }
                
                else if (indexLeft < left.Length)
                {
                    result[indexResult] = left[indexLeft];
                    indexLeft++;
                    indexResult++;
                }
                
                else if (indexRight < right.Length)
                {
                    result[indexResult] = right[indexRight];
                    indexRight++;
                    indexResult++;
                }  
            }
            return result;
        }

    }   
}