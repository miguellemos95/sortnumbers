using AutoMapper;
using NumbersOrdering.Dtos;
using NumbersOrdering.Models;

namespace NumbersOrdering.Profiles
{
    public class NumbersLineProfile : Profile
    {
        public NumbersLineProfile()
        {
            CreateMap<NumbersLine, NumbersLineReadDto>();
            CreateMap<NumbersLinePostDto, NumbersLine>();
        }
    }
}