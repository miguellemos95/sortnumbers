namespace NumbersOrdering.Dtos
{
    public class NumbersLinePostDto
    {
        public int[] numbers{get; set;}
        public long bubbleSortTime{get;set;}
        public long quickSortTime{get;set;}
        public long mergeSortTime{get;set;}
    }
}