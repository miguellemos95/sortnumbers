using Moq;  
using sortNumbers.Data;  
using sortNumbers.Models;  
using sortNumbers.Dtos;  
using sortNumbers.Controllers; 
using Xunit;  
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

namespace sortNumbersTest
{
    public class UnitTest1
    {
        public Mock<INumbersLineRepository> mock = new Mock<INumbersLineRepository>();  
        
        [Fact]
        public void getNumbers()
        {         
            NumbersLine numbersLine = new NumbersLine();
            NumbersLineReadDto numbersLineReadDto = new NumbersLineReadDto();

            int[] myNum = {10, 20, 30, 40};
            numbersLine.numbers = myNum;

            mock.Setup(p => p.GetNumbersLine()).Returns(numbersLine);

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<NumbersLine, NumbersLineReadDto>();
                cfg.CreateMap<NumbersLineReadDto, NumbersLine>();
            });

            IMapper _mapper = config.CreateMapper(); 
            NumbersLineController NumbersLineController = new NumbersLineController(mock.Object, _mapper); 
           
            var actionResult = NumbersLineController.Get();  
            var result = (ViewResult) actionResult.Result;
            var numbers = (NumbersLineReadDto) result.Model;
            Assert.Equal(numbersLine.numbers, numbers.numbers); 
        }

        [Fact]
        public void postNumbers()
        {
            NumbersLinePostDto numbersLinePostDto = new NumbersLinePostDto();
            NumbersLine expectNumbersLine = new NumbersLine();

            numbersLinePostDto.numberstext = "20, 30, 10, 40";
            int[] myNum = {10, 20, 30, 40};
            expectNumbersLine.numbers = myNum;

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<NumbersLine, NumbersLinePostDto>();
                cfg.CreateMap<NumbersLinePostDto, NumbersLine>();
            });

            IMapper _mapper = config.CreateMapper(); 
            
            NumbersLineController NumbersLineController = new NumbersLineController(mock.Object, _mapper);
           
            var actionResult = NumbersLineController.Post(numbersLinePostDto);  
            var result = (ViewResult)actionResult.Result;
            var resultNumbersLine = (NumbersLine) result.Model;

            Assert.Equal(expectNumbersLine.numbers, resultNumbersLine.numbers); 
        }
    }
}
