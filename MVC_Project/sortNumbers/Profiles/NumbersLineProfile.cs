using AutoMapper;
using sortNumbers.Dtos;
using sortNumbers.Models;

namespace sortNumbers.Profiles
{
    public class NumbersLineProfile : Profile
    {
        public NumbersLineProfile()
        {
            CreateMap<NumbersLine, NumbersLineReadDto>();
            CreateMap<NumbersLinePostDto, NumbersLine>();
        }
    }
}