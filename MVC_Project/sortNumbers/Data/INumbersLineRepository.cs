using sortNumbers.Models;
using System.Collections.Generic;

namespace sortNumbers.Data
{
    public interface INumbersLineRepository
    {
        NumbersLine PostNumbers(NumbersLine numbersLine);

        NumbersLine GetNumbersLine();
    }
}