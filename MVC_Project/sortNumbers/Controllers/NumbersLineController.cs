using System;
using Microsoft.AspNetCore.Mvc;
using sortNumbers.Models;
using sortNumbers.Data;
using sortNumbers.Dtos;
using AutoMapper;

namespace sortNumbers.Controllers
{
    public class NumbersLineController : Controller
    {
        private readonly INumbersLineRepository _repository;
        private readonly IMapper _mapper;

        public NumbersLineController(INumbersLineRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult <NumbersLine> Index()
        {      
            return View();
        }

        [HttpGet]
        public ActionResult <NumbersLineReadDto> Get()
        {
            NumbersLine numbersLine = _repository.GetNumbersLine();
            NumbersLineReadDto numbersLineReadDto = _mapper.Map<NumbersLineReadDto>(numbersLine);
            return View(numbersLineReadDto);
        }

        public ActionResult <NumbersLinePostDto> Post()
        {
            return View();
        }

        public ActionResult <NumbersLine> Help()
        {
            return View();
        }

        [HttpPost]
        public ActionResult <NumbersLinePostDto> Post(NumbersLinePostDto numbersLinePostDto)
        {
            NumbersLine numbersLine = _mapper.Map<NumbersLine>(numbersLinePostDto);
            NumbersLine response = null;

            if (ModelState.IsValid && numbersLine.numberstext!=null)
            {
                numbersLine.arrayFromString();
                if(numbersLine.numbers!=null)
                {
                    NumbersLine bubbleArray = numbersLine;
                    NumbersLine quickArray = numbersLine;
                    NumbersLine mergeArray = numbersLine;

                    bubbleArray.bubbleSort();
                    quickArray.quickSort();
                    mergeArray.mergeSort();

                    if(!bubbleArray.numbers.Equals(quickArray.numbers) || 
                       !bubbleArray.numbers.Equals(mergeArray.numbers))
                    {
                        throw new Exception("Arrays are different!");
                    }

                    response = mergeArray;
                    response.quickSortTime = quickArray.quickSortTime;
                    response.bubbleSortTime = bubbleArray.bubbleSortTime;
                    _repository.PostNumbers(response);
                }
            }

            return View(response);
        }
    }
}