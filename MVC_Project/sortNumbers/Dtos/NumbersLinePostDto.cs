using System;
using System.ComponentModel.DataAnnotations;
namespace sortNumbers.Dtos
{
    public class NumbersLinePostDto
    {
        public int[] numbers{get; set;}
        
        [Required (ErrorMessage = "This field cannot be empty!")]
        [RegularExpression(@"([0-9]+, ?)*[0-9]+", ErrorMessage = "Invalid input! (ex: 2,3,1)")]
        public string numberstext{get; set;}
        public long bubbleSortTime{get;set;}
        public long quickSortTime{get;set;}
        public long mergeSortTime{get;set;}
    }
}